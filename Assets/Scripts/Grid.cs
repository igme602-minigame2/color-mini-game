﻿using UnityEngine;
using System.Collections;


public class Grid : MonoBehaviour {

    #region Serialize Fields

    /// <summary>
    /// Tile Sprite
    /// </summary>
    public Sprite tileSprite;

    /// <summary>
    /// Number of rows in the grid
    /// </summary>
    public int rows;

    /// <summary>
    /// Number of colmns in the grid
    /// </summary>
    public int columns;

    /// <summary>
    /// size of the grid
    /// </summary>
    public Vector2 size;

    public Vector2 tileSize { get { return new Vector2(size.x / columns, size.y / rows); } }

    #endregion

    #region Properties

    public Tile[,] Tiles { get; private set; }

    #endregion

    #region Messages

    protected void Awake () {
        //Setup();
	}
	
	private void Update () {

    }

    #endregion

    #region Helpers

    public void Setup() {
        // remove all children
        // TODO: use object pool!
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        // we will need to scale the sprites according to the grid size and default sprite size
        var tileSize = tileSprite.bounds.size;

        var scale = new Vector2(size.x, size.y);
        scale.x /= tileSize.x * columns;
        scale.y /= tileSize.y * rows;
        
        Tiles = new Tile[rows, columns];

        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                // create a tile object
                // TODO: get from object pool
                var name = string.Format("{0}-{1}", r, c);
                var obj = new GameObject(name);
				obj.layer = gameObject.layer;
                obj.transform.parent = transform;

                var sr = obj.AddComponent<SpriteRenderer>();
                sr.sprite = tileSprite;
                // TODO: set proper color
                //sr.color = ((c + r) % 2 == 0) ? Color.white : Color.black;
                /*
                 * var rgb = new Color(0, 0, 0, 1);
                 * var channel = Random.Range(0, 3);
                 * rgb[channel] = Random.Range(0f, .5f);

                channel = (channel + Random.Range(0, 2)) % 3;
                rgb[channel] = Random.Range(.5f, 1f);
				*/

				var rcolor = Random.Range(0.0f, 1.0f);

				float hue = rcolor;
				//Debug.Log("HUE: "+hue);
				HSBColor color = new HSBColor(hue,0.8f,0.85f,1f);
				var rgb = HSBColor.ToColor(color);

                var tile = obj.AddComponent<Tile>();
                tile.row = r;
                tile.column = c;
                tile.color = rgb;
                Tiles[r, c] = tile;

                obj.AddComponent<BoxCollider>();
                // scale and position the tiles in the grid
                var pos = new Vector3 {
                    x = ((c + .5f) * tileSize.x * scale.x) - (size.x * .5f), 
                    y = ((r + .5f) * tileSize.y * scale.y) - (size.y * .5f)
                };

                obj.transform.localPosition = pos;
                obj.transform.localScale = scale;
            }
        }

    }

    public Color ApplyAction( Action action )
    {
        var t1 = Tiles[action.row, action.column];
        Tile t2 = null;

        if (action.direction == Vector2.up && action.row + 1 < rows)
        {
            t2 = Tiles[action.row+1, action.column];
        }
        else if (action.direction == Vector2.right && action.column + 1 < columns)
        {
            t2 = Tiles[action.row, action.column+1];
        }
        else if (action.direction == -Vector2.up && action.row > 0)
        {
            t2 = Tiles[action.row - 1, action.column];
        }
        else if (action.direction == -Vector2.right && action.column > 0)
        {
            t2 = Tiles[action.row, action.column - 1];
        }
        
        /* The ActionValid is used to confirm if the tile is present or not, 
         * in order to avoid any exceptions thrown 
         * during runtime */

        var color = new Color(0, 0, 0, 0);
        if (t2!=null)
        {
            var h1 = ((HSBColor)t1.color).h;
            var h2 = ((HSBColor)t2.color).h;

            float averageHue = (h1 + h2) * 0.5f;
            var delta = Mathf.Abs(h1 - h2);

            if (delta > 0.5f)
            {
                averageHue += 0.5f;
                if (averageHue < 0)
                {
                    averageHue += 1.0f;
                }
				else if (averageHue > 1) {
					averageHue -= 1.0f;
				}
            }
			averageHue = (float) decimal.Round((decimal)averageHue,2);
			//Debug.Log("HUE 1: " + h1);
			//Debug.Log("HUE 2: " + h2);

			//Debug.Log("AVG HUE: " + averageHue);
            color = HSBColor.ToColor(new HSBColor(averageHue, 1, 1));
            t2.BlendTo(color);
        }

        return color;
    }

	public Grid Clone(string cloneName, string layerName, Vector3 position, Vector3 scale)
    {
        var go = new GameObject();
        go.name = cloneName;
		go.layer = LayerMask.NameToLayer(layerName);

        var grid = go.AddComponent<Grid>();
        grid.tileSprite = tileSprite;
        grid.rows = rows;
        grid.columns = columns;
        grid.size = size;
        
        grid.Tiles = new Tile[rows, columns];

        foreach (Transform child in transform)
        {
            // TODO: get from object pool
            var obj = new GameObject(child.name);
			obj.layer = go.layer;
            obj.transform.parent = grid.transform;

            var childSr = child.GetComponent<SpriteRenderer>();
            var sr = obj.AddComponent<SpriteRenderer>();
            sr.sprite = tileSprite;

            var childTile = child.GetComponent<Tile>();
            var tile = obj.AddComponent<Tile>();
            tile.row = childTile.row;
            tile.column = childTile.column;
            tile.color = childTile.color;
            grid.Tiles[tile.row, tile.column] = tile;

            obj.transform.localPosition = child.transform.localPosition;
            obj.transform.localScale = child.transform.localScale;
        }

        grid.transform.position = position;
        grid.transform.localScale = scale;

        return grid;
    }

    #endregion

    #region Convenience

    /// <summary>
    /// Determines if tiles are adjacent
    /// </summary>
    /// <param name="tile1">Starting tile</param>
    /// <param name="tile2">Landing tile</param>
    public bool TilesAdjacent(Tile tile1, Tile tile2)
    {
        // Bail if tile doesn't exist
        if (!tile1 || !tile2)
        {
            return false;
        }

        // Bail if tiles are the same
        if (tile1.row == tile2.row && tile1.column == tile2.column)
        {
            return false;
        }

        bool adjacentByRow = (Mathf.Abs(tile1.row - tile2.row) == 1 && Mathf.Abs(tile1.column - tile2.column) == 0);
        bool adjacentByColumn = (Mathf.Abs(tile1.row - tile2.row) == 0 && Mathf.Abs(tile1.column - tile2.column) == 1);
        return (adjacentByRow || adjacentByColumn);
       
    }

   
}
    #endregion
