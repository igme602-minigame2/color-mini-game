﻿using UnityEngine;
using System.Collections;

public class Grid_old : MonoBehaviour {

    #region Serialize Fields

    /// <summary>
    /// Tile Sprite
    /// </summary>
    public Sprite tileSprite;

    /// <summary>
    /// Number of rows in the grid
    /// </summary>
    public int rows;

    /// <summary>
    /// Number of colmns in the grid
    /// </summary>
    public int columns;

    /// <summary>
    /// size of the tiles
    /// </summary>
    public Vector2 tileSize;

    #endregion

    #region Properties

    public Tile[,] Tiles { get; private set; }

    #endregion

    #region Messages

    protected void Awake () {
        Setup();
	}
	
	private void Update () {
       
    }

    #endregion

    #region Helpers

    private void Setup() {
        
        // remove all children
        // TODO: use object pool!
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        // we will need to scale the sprites according to the grid size and default sprite size
        //var gridSize = tileSprite.bounds.size;
        var gridSize = new Vector2(tileSize.x * columns, tileSize.y * rows);

        var scale = new Vector2(tileSize.x, tileSize.y);
        //scale.x /= gridSize.x * columns;
        //scale.y /= gridSize.y * rows;

        Tiles = new Tile[rows, columns];

        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                // create a tile object
                // TODO: get from object pool
                var name = string.Format("{0}-{1}", r, c);
                var obj = new GameObject(name);
                obj.transform.parent = transform;

                var sr = obj.AddComponent<SpriteRenderer>();
                sr.sprite = tileSprite;

                // TODO: set proper color
                var rgb = new Color(0, 0, 0, 1);
                var channel = Random.Range(0, 3);
                rgb[channel] = Random.Range(0f, .5f);

                channel = (channel + Random.Range(0, 2)) % 3;
                rgb[channel] = Random.Range(.5f, 1f);

                sr.color = rgb;
                //sr.color = ((c + r) % 2 == 0) ? Color.white : Color.black;

                var tile = obj.AddComponent<Tile>();
                Tiles[r, c] = tile;

                var col = obj.AddComponent<BoxCollider2D>();

                // scale and position the tiles in the grid
                var pos = new Vector3 {
                    x = (((c + .5f) * tileSize.x) - (gridSize.x * .5f)) * 0.01f,
                    y = (((r + .5f) * tileSize.y) - (gridSize.y * .5f)) * 0.01f
                };

                obj.transform.position = pos;
                obj.transform.localScale = scale;
            }
        }

    }

    #endregion
}
