﻿using UnityEngine;


public class Action
{
    /// <summary>
    /// The tile position
    /// </summary>
    public int row;

    public int column;

    public Tile landingTile;
    public Tile startingTile;
    
	public Color undoColor;

    /// <summary>
    /// The action direction
    /// </summary>
    public Vector2 direction;


    #region function members
   
    public void initValues(Tile fromTile, Tile toTile)
    {

        startingTile = fromTile;
        landingTile = toTile;
		     
        row = fromTile.row;
        column = fromTile.column;
		undoColor = landingTile.color;

        getDirection();
    }

    /// <summary>
    /// Determines the vector between the start tile and end tile
    /// </summary>
    /// <param name="fromTile">Starting tile</param>
    /// <param name="toTile">Landing tile</param>
    private void getDirection()
    {
        int rowDelta = row - landingTile.row;
        int columnDelta = column - landingTile.column;
        if (row == landingTile.row && column == landingTile.column)
        {
            direction = Vector2.zero;
        }

        if (rowDelta > 0)
        {
            direction = -Vector2.up;
        }
        else if (rowDelta < 0)
        {
            direction = Vector2.up;
        }
        else if (columnDelta > 0)
        {
            direction = -Vector2.right;
        }
        else // (columnDelta < 0)
        {
            direction = Vector2.right;
        }
    }
    #endregion
}