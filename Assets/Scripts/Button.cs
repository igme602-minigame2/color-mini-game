﻿using UnityEngine;
using System.Collections;



public class Button : MonoBehaviour {

	public event System.Action<Button> MouseDown;

	void Start () {
		gameObject.AddComponent<BoxCollider2D> ();
	}
	
	void Update () {
	
	}

	private void OnMouseDown()
	{
		if (MouseDown != null) {
			MouseDown (this);
		}
	}
}
