﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LevelManager : MonoBehaviour {

    public Grid grid;
    public Grid endState;
    public int puzzleSize;
    public int totalMoves;
	public Vector2 endStatePosition;
	public float endStateScale;

    private Stack<Action> userActions;

    private bool tracking;
    private Tile trackTile;

    private bool puzzleSolved;

	private Camera gridCamera;
    private SpriteRenderer feedbackTile;
    private Quaternion targetRotation;
    private Color targetColor;
    private Vector3 targetScale;
    private Vector3 targetPosition;
    private Vector3 trackScale;
    private Vector3 trackPosition;

	private GUIManager guiManager;

	private GameManager gameManager;

    protected virtual void Awake()
    {
        var a = GameManager.Instance;
		gridCamera = GameObject.FindGameObjectWithTag ("GridCamera").GetComponent<Camera>();
		gameManager = GameManager.Instance;

    }

    protected virtual void Start()
	{

		var randomLevel = gameManager.CurrentLevel == null;


		if (!randomLevel) {
			grid.rows = (int)gameManager.CurrentLevel.gridSize.y;
			grid.columns = (int)gameManager.CurrentLevel.gridSize.x;
		}

        grid.Setup();
        puzzleSize = grid.rows;
        //totalMoves = 5;
        
		guiManager = GetComponent<GUIManager> ();

        userActions = new Stack<Action>();

        var feedbackObj = new GameObject("Feedback");
        feedbackObj.layer = grid.gameObject.layer;
        feedbackTile = feedbackObj.AddComponent<SpriteRenderer>();
        feedbackTile.sprite = grid.tileSprite;
        feedbackTile.gameObject.SetActive(false);

        targetColor = new Color(0, 0, 0, 0);
        targetRotation = Quaternion.Euler(Vector3.zero);

		if (randomLevel){
			CreateRandomEndState(totalMoves);
		}
		else {
			CreateEndState (CreateActionsFromInfo (gameManager.CurrentLevel.actions));
		}
    }

    protected virtual void Update()
    {
        if (puzzleSolved)
        {
			gameManager.CurrentlevelIndex += 1;
            Application.LoadLevel(Application.loadedLevel);
        }

        feedbackTile.transform.localScale = Vector3.Lerp(feedbackTile.transform.localScale, targetScale, Time.deltaTime * 10);
        feedbackTile.transform.rotation = Quaternion.Lerp(feedbackTile.transform.rotation, targetRotation, Time.deltaTime * 10);
        feedbackTile.transform.position = Vector3.Lerp(feedbackTile.transform.position, targetPosition, Time.deltaTime * 10);
        feedbackTile.color = Color.Lerp(feedbackTile.color, targetColor, Time.deltaTime * 4);

        if (Input.GetMouseButtonDown(0))
        {
            trackTile = GetTileAtMousePosition(Input.mousePosition);
            tracking = trackTile != null;

            if (tracking)
            {
				trackPosition = trackTile.transform.position;
				trackScale = trackTile.transform.lossyScale;

				var color = trackTile.color;
                color.a = .5f;
                feedbackTile.color = color;
                feedbackTile.transform.position = trackPosition;
                feedbackTile.gameObject.SetActive(true);

                targetColor = color;
                targetPosition = trackPosition + new Vector3(0, 0, -.5f);
                targetRotation = Quaternion.Euler(Vector3.zero);
            }
        }
        
        if (Input.GetMouseButton(0))
        {
            if (tracking)
            {
                var layerMask = 1 << LayerMask.NameToLayer("Grid");
                var ray = gridCamera.ScreenPointToRay(Input.mousePosition);

                Vector3 angle = Vector3.zero;
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
                {
                    var hitPoint = hitInfo.point;
                    var hitTile = hitInfo.collider.GetComponent<Tile>();
                    var distance = hitPoint - trackPosition;
                    targetScale = trackScale * 1.25f;
                    targetPosition = trackPosition + new Vector3(0, 0, -.5f);
                    

                    if (hitTile == trackTile)
                    {
                        angle.x = distance.y / (grid.tileSize.y * .5f) * 20;
                        angle.y = -distance.x / (grid.tileSize.x * .5f) * 20;
                    }
                    else
                    {
                        var horizontal = distance.x * distance.x > distance.y * distance.y;
                        angle.x = !horizontal ? (distance.y > 0 ? 20 : -20) : 0;
                        angle.y = horizontal ? (distance.x > 0 ? -20 : 20) : 0;
                    }
                }
                else
                {
                    targetScale = trackScale;
                    targetPosition = trackPosition;
                }

                targetRotation = Quaternion.Euler(angle);
            }
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            // Bail if not tracking mouse movement
            if (!tracking)
            {
                return;
            }
			
			targetRotation = Quaternion.Euler(Vector3.zero);
			targetScale = trackScale;
			targetPosition = trackTile.transform.position;
			targetColor = trackTile.color;

            // Bail if we don't have a candidate for an action
            var adjacentTile = GetAdjacentTile(trackTile, Input.mousePosition);
            if (adjacentTile == null)
            {
                return;
            }

            var color = PerformColorMixWithTiles(trackTile, adjacentTile);
            color.a = .5f;

            targetPosition = adjacentTile.transform.position;
            targetColor = color;

            tracking = false;
            trackTile = null;
			guiManager.moves++;
			guiManager.UpdateMoveLabel();
            CheckEndState();
        }
    }

	private Action[] CreateActionsFromInfo (IList<ActionInfo> actionInfoList)
	{
		var actions = new Action[actionInfoList.Count];
		for (int index = 0, len = actionInfoList.Count; index < len; index++) {
			var actionInfo = actionInfoList [index];
			var action = new Action ();
			var fromTile = grid.Tiles [(int)actionInfo.startTile.y, (int)actionInfo.startTile.x];
			var toTile = grid.Tiles [(int)actionInfo.landTile.y, (int)actionInfo.landTile.x];
			action.initValues (fromTile, toTile);
			actions [index] = action;
		}

		return actions;
	}

    /// <summary>
    /// Use ray cast to find a tile
    /// </summary>
    /// <param name="mousePosition">Mouse Position</param>
    private Tile GetTileAtMousePosition(Vector3 mousePosition)
    {
        var layerMask = 1 << LayerMask.NameToLayer("Grid");

        var ray = gridCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, Mathf.Infinity, layerMask))
        {
            return raycastHit.collider.gameObject.GetComponent<Tile>() as Tile;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Use ray cast to find a tile
    /// </summary>
    /// <param name="mousePosition">Mouse Position</param>
    private Tile GetAdjacentTile(Tile target, Vector3 mousePosition)
    {
        var layerMask = 1 << LayerMask.NameToLayer("Grid");
        var ray = gridCamera.ScreenPointToRay(Input.mousePosition);

        Vector3 angle = Vector3.zero;
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
        {
            var hitPoint = hitInfo.point;
            var hitTile = hitInfo.collider.GetComponent<Tile>();
            var distance = hitPoint - trackPosition;

            if (hitTile == target)
            {
                return null;
            }

            var row = target.row;
            var col = target.column;

            var horizontal = distance.x * distance.x > distance.y * distance.y;
            if (horizontal)
            {
                col += (int)Mathf.Sign(distance.x);
            }
            else
            {
                row += (int)Mathf.Sign(distance.y);
            }

            return grid.Tiles[row, col];
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Create an action instance applies it to the grid
    /// </summary>
    /// <param name="tile1">Starting tile</param>
    /// <param name="tile2">Landing tile</param>
    protected virtual Color  PerformColorMixWithTiles(Tile tile1, Tile tile2)
    {

        var action = new Action();
        action.initValues(tile1, tile2);
        
        var color = grid.ApplyAction(action);

        //The action is pushed onto a stack from where we can undo the actions.
        userActions.Push(action);
        return color;
    }

    private void CheckEndState()
    {
        for (int row = 0; row < grid.rows; row++)
        {
            for (int column = 0; column < grid.columns; column++)
            {
                var tile1 = grid.Tiles[row, column];
                var tile2 = endState.Tiles[row, column];

                var hue1 = ((HSBColor)tile1.color).h;
                var hue2 = ((HSBColor)tile2.color).h;
                if (Mathf.Abs(hue2 - hue1) > 0.03f)
                {
                    // TO FUCKING DO: Blink this son of a non matching bitch!
                    Debug.Log("r " + row + " c " + column + " diff " + (hue2 - hue1));
                    return;
                }
            }
        }

        puzzleSolved = true;
    }

    /// <summary>
    /// Create an end state grid with random moves
    /// </summary>
    /// <param name="moves">number of random moves to execute</param>
    private void CreateRandomEndState(int moves = 4)
    {
        // TODO: create an array of random actions

        Action[] actions = new Action[moves];
        System.Random random = new System.Random();
        int randomNumber;
        int limit = 1; 

        // this matrix stores the number of changes made to each tile
        int[,] totalChangesMade = new int[puzzleSize,puzzleSize];

        // initialize the matrix to 0
        for(int i=0;i<puzzleSize;i++)
        {
            for(int j=0;j<puzzleSize;j++)
            {
                totalChangesMade[i,j] = 0;
            }
        }


        for (int i = 0; i < moves; i++)
        {
            actions[i] = new Action();

            // puzzleSize defined above

            //this checks the matrix to see if the tile already been modified inthis iteration
            do
            {
                randomNumber = random.Next(puzzleSize);
                actions[i].row = randomNumber;

                randomNumber = random.Next(puzzleSize);
                actions[i].column = randomNumber;

                totalChangesMade[actions[i].row, actions[i].column]++;

                if(limit != (i/(puzzleSize*(puzzleSize-1) +1)))
                {
                    limit++;
                }
            
            // If the total number of moves is greater than total number of tiles then increase the limit but only after all the tiles have been moved atleast once. 
            } while (totalChangesMade[actions[i].row, actions[i].column] > limit);

            randomNumber = Random.Range(0, 4);
            switch(randomNumber)
            {
                case 0:
                    actions[i].direction = Vector2.up;
                    break;
                case 1:
                    actions[i].direction = -Vector2.up;
                    break;
                case 2:
                    actions[i].direction = Vector2.right;
                    break;
                case 3:
                    actions[i].direction = -Vector2.right;
                    break;
            }

			// To avoid producing any invalid actions
         
            if (actions[i].row == 0 && actions[i].direction == -Vector2.up) { actions[i].direction = Vector2.up;  }

            if (actions[i].row == (puzzleSize - 1) && actions[i].direction == Vector2.up) { actions[i].direction = -Vector2.up;  }

            if (actions[i].column == 0 && actions[i].direction == -Vector2.right) { actions[i].direction = Vector2.right; }

            if (actions[i].column == (puzzleSize - 1) && actions[i].direction == Vector2.right) { actions[i].direction = -Vector2.right; }
         
            Debug.Log(actions[i].row + " " + actions[i].column + " " + randomNumber);
        }

        CreateEndState(actions);
    }

    /// <summary>
    /// Create an end state grid
    /// </summary>
    /// <param name="moves">number of random moves to execute</param>
    private void CreateEndState(Action[] actions)
    {
        endState = grid.Clone("End State", "UI", endStatePosition,  Vector3.one * endStateScale);

        int numberOfActions = actions.Length;

        for (int i = 0; i < numberOfActions; i++) { endState.ApplyAction(actions[i]); }

		guiManager.perfect = actions.Length;
		guiManager.UpdateGoalLabel ();
    }

	public void Restart() {
		while (userActions.Count > 0) {
			Undo();
		}

	}

	public void Undo(){
		var lastAction = userActions.Pop ();

		grid.Tiles [lastAction.landingTile.row, lastAction.landingTile.column].BlendTo (lastAction.undoColor);

		guiManager.moves -= 1; 
		guiManager.UpdateMoveLabel ();
		feedbackTile.gameObject.SetActive (false);
	}

	public void Quit() {
		Application.Quit ();
	}


}
