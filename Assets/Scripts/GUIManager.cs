﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {
	
	public float moves;
	public float perfect;
	public GUIText restart, undo, quit, moveLabel, goalLabel,help,helpColor,helpGoal;
	private bool helpCalled;

	void Start () 
	{
		Button restartButton = restart.GetComponent<Button>();
		restartButton.MouseDown += OnRestartTap;

		Button undoButton = undo.GetComponent<Button>();
		undoButton.MouseDown += OnUndoTap;

		Button quitButton = quit.GetComponent<Button>();
		quitButton.MouseDown += OnQuitTap;

		Button helpButton = help.GetComponent<Button> ();
		helpButton.MouseDown += OnHelpTap;

		helpCalled = false;
		helpColor.text = "";
		helpGoal.text = "";

		UpdateMoveLabel ();
	}

	protected virtual void OnRestartTap(Button button)
	{
		LevelManager levelManager = GetComponent<LevelManager> ();
		levelManager.Restart ();
	}

	protected virtual void OnUndoTap(Button button)
	{
		LevelManager levelManager = GetComponent<LevelManager> ();
		levelManager.Undo ();
	}

	protected virtual void OnQuitTap(Button button)
	{
		LevelManager levelManager = GetComponent<LevelManager> ();
		Debug.Log (levelManager);

		levelManager.Quit ();
	}

	protected virtual void OnHelpTap(Button button)
	{
		
		Debug.Log ("help is called ");
		if (helpCalled == false)
		{
			helpColor.text = "Change the color of the tiles by clicking and dragging";
			helpGoal.text = "Objective:   make left grid similar to the right grid";
			helpCalled = true;
		} else 
		{
			helpColor.text = "";
			helpGoal.text = "";
			helpCalled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void UpdateMoveLabel()
	{
		moveLabel.text = "moves: " + moves;
	}

	public void UpdateGoalLabel()
	{
		goalLabel.text = "perfect: " + perfect;
	}

}
