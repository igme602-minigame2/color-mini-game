﻿using Assets.Scripts.Engine;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : ScriptableObject
{

    #region Fields

    [NonSerialized]
    public int CurrentlevelIndex;

    public Color[] Colors;
    public LevelInfo[] TutorialLevels;

    #endregion

    #region Properties

    public LevelInfo CurrentLevel { get { return CurrentlevelIndex >= TutorialLevels.Length ? null : TutorialLevels[CurrentlevelIndex]; } }

    #endregion

    #region Messages

    protected void OnEnable()
    {

        //Debug.Log("Game configuration file loaded!");
    }

    #endregion

    #region Public API

    #endregion

    #region Singleton

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            instance = instance ?? ScriptableResource.Load<GameManager>("GameManager");
            return instance;
        }
    }
    #endregion
}

[Serializable]
public class LevelInfo
{
    public string name;
    public Vector2 gridSize;
    public List<ActionInfo> actions;
}

[Serializable]
public class ActionInfo
{
    public Vector2 startTile;
	public Vector2 landTile;
}