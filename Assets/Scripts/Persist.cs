﻿using UnityEngine;

public class Persist : MonoBehaviour {

    private static Persist instance;

    void Awake()
    {
        
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        GameObject.DontDestroyOnLoad(this);
    }
}
