﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

    private Color targetColor;

    public int row, column;
    public SpriteRenderer spriteRenderer { get { return GetComponent<SpriteRenderer>(); } }

    public Color color
    {
        get
        {
            return targetColor;
        }
        set
        {
            spriteRenderer.color = targetColor = value;
        }
    }


    private void Setup(Color color)
    {

    }

    private void Update()
    {
        spriteRenderer.color = Color.Lerp(spriteRenderer.color, targetColor, Time.deltaTime * 4);
    }

    internal void BlendTo(Color color)
    {
        targetColor = color;
    }
}
